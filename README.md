# folio

A script that will calculate the current value of a stock market portfolio

#### Stocks Quotation Service

Via Browser
The easiest way to test out the Intrinio API is by using your browser.

Paste your request into the address bar. Be sure to include your API Key by specifying an api_key URL parameter set to the value of your API Key. For example:

https://api-v2.intrinio.com/companies/AAPL?api_key=OjMwMTIwZDE1NWRkM2UzY2M4MjRiZmExMWVlZGFkNzY1

##### API keys:

- Sandbox
OjMwMTIwZDE1NWRkM2UzY2M4MjRiZmExMWVlZGFkNzY1

- Production
Ojg0NTk1OTM5NDVkMzJjMDljYjA4YzA2NGZjMzI2OTFl

##### Sample Output
`curl https://financialmodelingprep.com/api/v3/company/profile/AAPL | jq`
```json
{
  "symbol": "AAPL",
  "profile": {
    "price": 320.54,
    "beta": "1.228499",
    "volAvg": "28835583",
    "mktCap": "1.40251628E12",
    "lastDiv": "2.92",
    "range": "168.42-327.85",
    "changes": 1.69,
    "changesPercentage": "(+0.53%)",
    "companyName": "Apple Inc.",
    "exchange": "Nasdaq Global Select",
    "industry": "Computer Hardware",
    "website": "http://www.apple.com",
    "description": "Apple Inc is designs, manufactures and markets mobile communication and media devices and personal computers, and sells a variety of related software, services, accessories, networking solutions and third-party digital content and applications.",
    "ceo": "Timothy D. Cook",
    "sector": "Technology",
    "image": "https://financialmodelingprep.com/images-New-jpg/AAPL.jpg"
  }
}
```

###Challenge

#####DESCRIPTION

You’ll be working with a software engineering team to deliver business value. To demonstrate excellence in modern development tools and frameworks, we ask that you complete the following challenge. Please use this test to show off your skill set and what you can bring to the team. You will be critiqued on your quality, completeness, creativity, and technologies. If we proceed forward in the interviewing process, you will be asked to walk through your code. Unless otherwise stated in the challenge, choose modern technologies that exercise the breadth of approach and ones that you’re comfortable developing with.

When you have completed the following challenge, place your code in a code repository, ex. github, bitbucket, dropbox, etc.

#####MISSION

Write a script using NodeJS that will calculate the current value of a stock market portfolio if a person were to buy any number of shares of three separate stocks on 1/1/2019 when the market opened. Use any three stocks and quantities of your choosing, which can be statically defined in the script. Output as a CSV file. For each stock include the ticker, quantity bought, real-time price, highest price and lowest price between 1/1/2019 and now, and the calculated current value of that holding. The last row is a Total which only includes the current total value of the portfolio. Write Unit Tests for the script to appropriately test the code written (The Jest framework is popular).

#####REQUIREMENTS
    Use this API to retrieve stock prices: https://financialmodelingprep.com/developer/docs/

######Example Output:
```csv
Ticker,Quantity,Current Price,High,Low,Current Value

AAPL,5,$315.11,$323.33,$142.00,"$1,575.53"

SPY,10,$326.61,$327.39,$242.60,"$3,266.10"

KMI,15,$21.49,$21.88,$15.10,$322.35

Total,,,,,"$5,163.98"
```

### Developer Notes
- sample to the job locally:
`watchexec -w src  "./node_modules/.bin/babel-node -b  @babel/preset-env src/index.js -s AAPL SPY KMI -q 5 10 15"`
