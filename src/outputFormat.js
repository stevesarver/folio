/**
 * Return the value as American currency
 * @param value the number to format. Expected to look like: '225.13'
 * @return {string} '$225.13'
 */
import * as fs from 'fs'

export const asCurrency = value => {
  const numberValue = value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  return `$${numberValue}`
}

export const asCSV = input => {
  return input.map(({ticker, quantity, price, high, low, value}) => {
    const cells = [
      ticker,
      quantity,
      asCurrency(price),
      asCurrency(high),
      asCurrency(low),
      asCurrency(value)
    ]
    return escapeValues(cells)
  })
};
export const footer = input => {
  const total = [...input.map(({value}) => value)].reduce((a, b) => a + b, 0)
  return `Total,,,,,${escapeCellValue(asCurrency(total))}`
};

const HEADER = [
  `Ticker`,
  `Quantity`,
  `Current Price`,
  `High`,
  `Low`,
  `Current Value`
]

function escapeCellValue(cellValue) {
  const value = `${cellValue}`
  if (value.indexOf(' ') !== -1) {
    return `"${value}"`
  }
  if (value.indexOf(',') !== -1) {
    return `"${value}"`
  }
  return value;
}

let escapeValues = function (array) {
  return array.map(columnHeader => escapeCellValue(columnHeader)).join(',')
}
export const header = () => {
  return escapeValues(HEADER)
};

export const outputFile = (path, output) => {
  var file = fs.createWriteStream(path);
  file.on('error', function (err) {
    throw err
  });
  [...output].forEach(v => {
    file.write(v + '\n');
  });
  file.end();
}
