/**
 * Figure what the security is worth
 * @param rowData
 * @return {{value: *}}
 */
import moment from 'moment'

export const calculatePortfolio = ({price, quantity}) => {
  const value = price * quantity;
  return value
};

/**
 * Return the data for a given date
 * @param historical is an array of data
 * @param date is that for which we are interested
 */
export const findFromRange = (historical, date) => {
  const now = moment(date)
  const yesterday = now.subtract(1, 'days')
  const formattedDate = yesterday.format("YYYY-MM-DD");
  const filtered = historical.filter(row => row.date === formattedDate)
  if (filtered.length !== 1) {
    throw new Error('Unexpected. There should have been a record for yesterday')
  }
  return filtered[0]
}
/**
 * Calculate the values from the history
 * @param historical is an array of data
 * @return {{high: *, low: *}} each value
 */
export const calculateHighLowFromRange = historical => {
  const high = Math.max(...historical.map(({high}) => high))
  const low = Math.min(...historical.map(({low}) => low))
  return {high, low}
}


/**
 * Map that data returned from the external service.
 * We need these values:
 *  Ticker,Quantity,Current Price,High,Low,Current Value
 * @param response the raw JSON (@see test/stubs/quote.json for a sample)
 * @return {{ticker: *, high: *, low: *, price: *}}
 */
export const mapQuote = response => {
  const {data, stock} = response
  const {symbol, historical} = data
  const {ticker, quantity} = stock
  if (symbol.trim().toUpperCase() !== ticker.trim().toUpperCase()) {
    throw new Error(`The ticker symbol differs from the expected ${symbol} ${ticker}`)
  }
  if (historical.length < 5) {
    throw new Error(`The historical data is insufficient for ${ticker}.`)
  }
  const {high, low} = calculateHighLowFromRange(historical)
  const {open: price} = findFromRange(historical, new Date())
  const value = calculatePortfolio({price, quantity})
  return {ticker, price, high, low, quantity, value}
}
