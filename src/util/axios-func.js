import axios from 'axios'
import logSuccess from './middleware/log-success'
import logError from './middleware/log-error'
import checkHttpStatus from './middleware/check-status'

export const get = function (url, headers, logger) {
  return axios.get(`${url}`, { headers })
    .then(checkHttpStatus)
    .then(logSuccess(logger, { 'message': `[Success] - GET ${url}` }))
    .catch(logError(logger, { 'message': `[Error] - GET ${url}` }))
}
