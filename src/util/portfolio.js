export const getPortfolio = () => {
  const argv = require('yargs')
    .usage('Usage: $0 -s [ticker symbols] -q [share quantities]')
    .example('$0 -s AAPL INTC MS -q 300 150 88.5', 'Calculate a portfolio of up to 3 stocks')
    .alias('s', 'tickers')
    .nargs('s', 3)
    .describe('s', 'stock ticker symbols')
    .demandOption(['s'])
    .alias('q', 'quantities')
    .nargs('q', 3)
    .describe('q', 'share counts')
    .demandOption(['q'])
    .help('h')
    .alias('h', 'help')
    .argv
  const {q: quantities, s: symbols} = argv
  return syncArgs({symbols, quantities})
}

export const syncArgs = ({symbols, quantities}) => {
  if (symbols.length === quantities.length) {
    return symbols.map((symbol, index) => {
      return {'ticker': symbol, 'quantity': quantities[index]}
    })
  }
  throw Error('Illegal arguments')
}
