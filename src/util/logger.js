/**
 * I used this for development, but out of scope. Could replace with an NM specific library or maybe log4js.
  * https://www.npmjs.com/package/log4js
 *
 * This is just a placeholder for a more interesting implementation.
 * In a real system we would need something to do logging.
 * @type {{error: logger.error, info: logger.info}}
 */

export const logger = {
  info: (...args) => {
    console.dir(args)
  },
  error: (...args) => {
    console.log(args)
  }
}
