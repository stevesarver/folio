export default (log, message) => response => {
  log.info(message)
  return response
}
