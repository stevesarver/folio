export default (response) => {
  if (response.status >= 200 && response.status < 300) {
    return response
  }
  const error = new Error(response.statusText || 'Status not OK')
  error.response = response
  throw error
}
