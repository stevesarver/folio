export default (log, message, throwError = true, defaultReturn = null) => error => {
  const msg = error.response ? error.response.data : error
  const errorMessage = error.message || error.code
  log.error(Object.assign(message, { error: errorMessage, msg: msg }))

  if (throwError) {
    throw error
  }
  return defaultReturn
}
