import { get } from './axios-func'

const {domain, service, fromDate} = require('config')

/**
 * Pull a quote from the external API
 * @param stock the stock's ticker symbol
 * @param headers for the request
 * @param logger for tracking, auditing etc.
 * @return {Promise<T>}
 */
const getQuote = (stock, headers, logger) => {
  let url = `${domain}/${service}/${stock.ticker}?from=${fromDate}`
  return get(url, headers, logger).then((response) => {
    return {...response, stock}
  }).catch((error) => {
    throw error
  })
}

module.exports.getQuote = getQuote
