import { main} from './app'
import { getPortfolio } from './util/portfolio'

main(getPortfolio())
