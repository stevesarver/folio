import { getQuote } from './util/quoteService'
import { logger } from './util/logger'
import { mapQuote } from './util/dataTransforms'
import { asCSV, footer, header, outputFile } from './outputFormat'
import moment from 'moment'

export const main = async (portfolio) => {
  try {
    const promises = portfolio.map(async (securityData) => {
      return await getQuote(securityData, {}, logger)
    })
    const raw = await Promise.all(promises)
    const csvData = raw.map(data => {
      return mapQuote(data)
    })
    const output = []
    output.push(header(csvData))
    output.push(...asCSV(csvData))
    output.push(footer(csvData))

    const cout = []
    cout.push(`\`\`\`csv`)
    cout.push(...output)
    cout.push(`\`\`\``)
    logger.info(cout)

    const timestamp = moment().format('YYYYMMDDHHmmss')
    outputFile(`portfolio.${timestamp}.csv`, output)

  } catch (error) {
    const {stack: trace} = error
    logger.error(`Error generating Portfolio. ${trace}`)
  }
}

