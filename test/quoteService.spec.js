import axios from 'axios'
import { getQuote } from '../src/util/quoteService'
import { calculateHighLowFromRange, mapQuote } from '../src/util/dataTransforms'

// sample data pulled from the external API
const stub = require('./stubs/quote.json')
jest.mock('axios')
// mock logger, incidental to this code
const logger = {
  info: jest.fn(),
  error: jest.fn()
}

const data = [{ticker: 'AAA', price: 12.445432, quantity: 23.55234, high: 12.765, low: 100, value: 3.009153},
  {ticker: 'AAA', price: 12.445432, quantity: 23.55234, high: 12.765, low: 100, value: 2.009153},
  {ticker: 'AAA', price: 12.445432, quantity: 23.55234, high: 12.765, low: 100, value: 5.009153}]

describe('We can parse the stock range', () => {
  it('Are we deciphering the range correctly', () => {
    const {high, low} = calculateHighLowFromRange(data)
    expect(high).toBe(12.765)
    expect(low).toBe(100)
  })
})

describe('We can call the microservice for stock data', () => {
  it('We need to send in a stock ticker or fail', async () => {
    const getSuccess = {status: 200, message: true}
    getSuccess.data = stub
    axios.get.mockResolvedValue(getSuccess)
    const expected = {
      "high": 323.4,
      "low": 177.81,
      "price": 314.18,
      "quantity": 10,
      "ticker": "AAPL",
      "value": 3141.8
    }
    const results = await getQuote('/123', {}, logger)
    results.stock = {ticker: 'AAPL', quantity: 10}
    expect(mapQuote(results)).toEqual(expected)
  })
})
