import { syncArgs } from '../src/util/portfolio'

describe('Verify argument gathering', () => {
  it(`We can synchronize ticker symbols to share counts`, () => {
    const data = syncArgs({symbols: ["AAPL"], quantities: [200]})
    expect(data).toEqual([{
      "ticker": "AAPL",
      "quantity": 200
    }])
  })
  it(`We can synchronize ticker symbols to share counts`, () => {
    const data = syncArgs({symbols: ["AAPL", "IBM"], quantities: [200, 30.50]})
    expect(data).toEqual([
      {
        "ticker": "AAPL",
        "quantity": 200
      },
      {
        "ticker": "IBM",
        "quantity": 30.5
      }
    ])
  })
  it(`We can synchronize ticker symbols to share counts`, () => {
    expect(() => syncArgs(["AAPL", "IBM"], [])).toThrow(Error);
  })
})
