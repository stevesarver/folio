import axios from 'axios'
import { get } from '../../src/util/axios-func'

jest.mock('axios')

// mock logger
const logger = {
  info: jest.fn(),
  error: jest.fn()
}

describe('Axios Func Success', function () {
  it('should handle GET success', async () => {
    const getSuccess = {status: 200, message: true}
    axios.get.mockResolvedValue(getSuccess)
    await expect(get('/123', {}, logger)).resolves.toEqual(getSuccess)
  })
})

describe('Axios Func Failures', function () {
  it('should handle GET failure', async () => {
    const getSuccess = {status: 500, message: true}
    axios.get.mockResolvedValue(getSuccess)
    await expect(get('/123', {}, logger)).rejects.toThrow(Error)
  })

  it('should handle GET rejections', async () => {
    const getFailure = new Error(`GET failure`)
    axios.get.mockRejectedValue(getFailure)
    await expect(get('/123', {}, logger)).rejects.toThrow(getFailure)
  })
})
