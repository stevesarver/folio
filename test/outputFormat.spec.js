import { asCSV, asCurrency, footer, header } from '../src/outputFormat'

const data = [{ticker: 'AAA', price: 12.445432, quantity: 23.55234, high: 12.765, low: 100, value: 3.009153},
  {ticker: 'AAA', price: 12344312.445432, quantity: 123423.55234, high: 43212.765, low: 234100, value: 12342.009153},
  {ticker: 'AAA', price: 12.445432, quantity: 23.55234, high: 12.765, low: 100, value: 5.009153}]

describe('We can format currency', () => {
  it('Are we formatting the values correctly', () => {
    expect(asCurrency(1)).toBe('$1.00')
    expect(asCurrency(12)).toBe('$12.00')
    expect(asCurrency(123)).toBe('$123.00')
    expect(asCurrency(1234)).toBe('$1,234.00')
    expect(asCurrency(12345)).toBe('$12,345.00')
    expect(asCurrency(123456)).toBe('$123,456.00')
    expect(asCurrency(1234567)).toBe('$1,234,567.00')
    expect(asCurrency(12345.67)).toBe('$12,345.67')
  })
})
describe('body', () => {
  it('Is the format correct', () => {
    const actual = asCSV(data)
    expect(actual).toStrictEqual([
      "AAA,23.55234,$12.45,$12.77,$100.00,$3.01",
      "AAA,123423.55234,\"$12,344,312.45\",\"$43,212.76\",\"$234,100.00\",\"$12,342.01\"",
      "AAA,23.55234,$12.45,$12.77,$100.00,$5.01"
    ])
  })
})
describe('footer', () => {
  it('Is total valid', () => {
    expect(footer(data)).toStrictEqual("Total,,,,,\"$12,350.03\"")
  })
})
describe('header', () => {
  it('Is the static content valid', () => {
    expect(header()).toStrictEqual("Ticker,Quantity,\"Current Price\",High,Low,\"Current Value\"")
  })
})
