module.exports = {
  domain: 'https://financialmodelingprep.com',
  service: '/api/v3/historical-price-full',
  fromDate: '2019-01-01'
}
