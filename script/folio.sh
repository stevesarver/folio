#!/usr/bin/env bash

echo "Usage: ./folio.sh INTC:25 AAPL:100 IBM:78"
extract() {
    jq $1 <./folio/$2.json
}

echo
output=./$(date +%Y%m%d%H%M%S).csv
echo "Ticker,Quantity,Price,Value" >>${output}

for ARG in $*; do
    # curl https://financial.com/quote?AAPL > ./folio/$ticker.json
    quantity=$(echo $ARG | sed 's/.*://')
    ticker=$(echo $ARG | sed 's/:.*//')
    range=$(extract '.data.range' $ticker)
    price=$(extract '.data.price' $ticker | sed 's/\"//g')
    low=\$$(echo ${range} | sed 's/\-.*//' | sed 's/\"//g')
    high=\$$(echo ${range} | sed 's/.*\-//' | sed 's/\"//g')
    value=\$$(echo "${quantity} * ${price}" | bc -l)
    echo "${ticker}: ${quantity}*${price}=$value (${low}-${high})"
    echo "${ticker},${quantity},${price},$value" >>${output}
done
echo
echo "Results: ${output}:"
echo "-----------------------------"
bat ${output}
echo "-----------------------------"
